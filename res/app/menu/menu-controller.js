/**
* Copyright © 2019 contains code contributed by Orange SA, authors: Denis Barbaron - Licensed under the Apache license 2.0
**/

module.exports = function MenuCtrl(
  $scope
  , $rootScope
  , SettingsService
  , $location
  , $http
  , CommonService
  , LogcatService
  , socket
  , $cookies
  , $window
  , GenericModalService) {

    var email='';
    var password='';
    
    // code to restrict copy of content in portal
    // document.body.oncopy = new Function('return false');

    function getUserEmail(){
        let ssid = $cookies.get('ssid');
        let token = "e9."+ssid+".S"
        let jwt_decode = require("jwt-decode");
          var decoded = jwt_decode(token);
          document.getElementById("userloginDetail").innerText=decoded.jwt.name;
          email = decoded.jwt.name;
          password= decoded.jwt.email
    }
    getUserEmail()

    const getQRCode = (accessToken) => {
        console.log("acccestoken" ,accessToken )
      let url = `/auth/mock/mfa/qrcode?accessToken=${accessToken}`
      $http.get(url).then(uri=>{
        document.querySelector('#MFAQRCode').src = uri.data.uri
      })
    }

    const enableMFA = (accessToken, userCode) => {
 
        let uri =`/auth/mock/mfa/validate?accessToken=${accessToken}&userCode=${userCode}`
       
        $http.post(uri)
          .then((result) => {
            if (result.status === 200 && result.statusText === 'OK') {
              console.log("MFA enabled")
  
              const settings = {
                PreferredMfa: true,
                Enabled: true,
              }
              const Pool = require('../../auth/cognito/config/userpool');
              var AmazonCognitoIdentity = require("amazon-cognito-identity-js");
        
              var poolData = {
                UserPoolId: 'ap-south-1_9CNNhmZqD', // Your user pool id here
                ClientId: '4a7jtbl6lcg7hfih6u1pn2qu72', // Your client id here
              }
              var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData)
              var userData = {
                Username: email,
                Pool: userPool,
              }
              var user = new AmazonCognitoIdentity.CognitoUser(userData)
              user.setUserMfaPreference(null, settings, () => {})
            } else {
              console.log(result)
  
              if (result.errorType === 'EnableSoftwareTokenMFAException') {
                alert('Incorrect 6-digit code!')
              } else if (result.errorType === 'InvalidParameterException') {
                alert('Please provide a 6-digit number')
              }
            }
          })
          .catch(console.error)
    }


    const disableMFA = (userCode) => {
      const account = require('../../auth/cognito/config/accounts')
      account.getSession().then(({headers, accessToken}) => {
        if (typeof accessToken !== 'string') {
          accessToken = accessToken.jwtToken
        }
  
        const uri = `/auth/mock/mfa/validate?accessToken=${accessToken}&userCode=${userCode}`
        console.log(uri)
  
        fetch(uri, {
          method: 'DELETE',
          headers,
        })
          .then((data) => data.json())
          .then(console.log)
          .catch(console.error)
      })
    }

    // var MFATriggerButton = document.querySelector('.mfaContainer');
    // var closeMFATriggerButton = document.getElementById('closeQRCodeDisplayContainer');

    // MFATriggerButton.addEventListener('click',()=>{
    //   let keyID = `CognitoIdentityServiceProvider.4a7jtbl6lcg7hfih6u1pn2qu72.${email}.accessToken`;
    //   let accessToken = localStorage.getItem(keyID)
    //   getQRCode(accessToken)
    //   setTimeout(()=>{
    //     document.querySelector('.QRCodeDisplayContainer').style.display ="flex"; 
    //       document.querySelector('.mfaContainer').style.display="none";
    //   },2000)
    // })
    
    // closeMFATriggerButton.addEventListener('click',()=>{
    //   let userCode = document.querySelector('#verifyOTPCode').value;
    //   let keyID = `CognitoIdentityServiceProvider.4a7jtbl6lcg7hfih6u1pn2qu72.${email}.accessToken`;
    //   let accessToken = localStorage.getItem(keyID)
    //   new Promise(resolve=>{
    //     let result=enableMFA(accessToken,userCode);
    //     resolve(result)
    //   }).then(result=>{
    //     console.log(result)
    //     document.querySelector('.mfaContainer').style.display="flex";
    //     document.querySelector('.QRCodeDisplayContainer').style.display="none";
    //   })
    // })

    var inactivityTime = function () {
      var time;
      window.onload = resetTimer;
      // DOM Events
      document.onmousemove = resetTimer;
      document.onkeydown = resetTimer;

      function logout() {
        let cognito = require('../../auth/cognito/config/accounts');
        cognito.logout(email);
        window.localStorage.clear();
        let sid = $cookies.get('ssid');
        if (sessionStorage.getItem('session')) {
          var setTestId = sessionStorage.getItem("testId")
          GenericModalService.open({
            message: "Stopping session capture...",
            type: "Information",
            cancel: false,
            hideButton: true,
            addTestId: setTestId,
            stopSession: true
          })
            .then(response => {
              sessionStorage.removeItem('session')
              sessionStorage.removeItem('testId')
              $cookies.remove('XSRF-TOKEN', { path: '/' })
              $cookies.remove('ssid', { path: '/' })
              $cookies.remove('ssid.sig', { path: '/' })
              $window.location = '/auth/logout/'+`${sid}`
              setTimeout(function () {
                socket.disconnect()
              }, 100)
            })
        }
        else {
          $cookies.remove('XSRF-TOKEN', { path: '/' })
          $cookies.remove('ssid', { path: '/' })
          $cookies.remove('ssid.sig', { path: '/' })
          $window.location = '/auth/logout/'+`${sid}`
          setTimeout(function () {
            socket.disconnect()
          }, 100)
        }
      }

      function resetTimer() {
        clearTimeout(time);
        time = setTimeout(logout, 600000);
        // 1000 milliseconds = 1 second
      }
    }

    
      inactivityTime();

  

    

  SettingsService.bind($scope, {
    target: 'lastUsedDevice'
  })

  SettingsService.bind($rootScope, {
    target: 'platform',
    defaultValue: 'native',
    deviceEntries: LogcatService.deviceEntries
  })

  $scope.$on('$routeChangeSuccess', function () {
    $scope.isControlRoute = $location.path().search('/control') !== -1
  })

  $scope.mailToSupport = function () {
    CommonService.url('mailto:' + $scope.contactEmail)
  }

  $http.get('/auth/contact').then(function (response) {
    $scope.contactEmail = response.data.contact.email
  })

  $scope.logout = function () {
    // let cognito = require('../../auth/cognito/config/accounts');
    // cognito.logout(email);
    window.localStorage.clear();
    let sid = $cookies.get('ssid');
    if (sessionStorage.getItem('session')) {
      var setTestId = sessionStorage.getItem("testId")
      GenericModalService.open({
        message: "Stopping session capture...",
        type: "Information",
        cancel: false,
        hideButton: true,
        addTestId: setTestId,
        stopSession: true
      })
        .then(response => {
          sessionStorage.removeItem('session')
          sessionStorage.removeItem('testId')
          $cookies.remove('XSRF-TOKEN', { path: '/' })
          $cookies.remove('ssid', { path: '/' })
          $cookies.remove('ssid.sig', { path: '/' })
          $window.location = '/auth/logout/'+`${sid}`
          setTimeout(function () {
            socket.disconnect()
          }, 100)
        })
    }
    else {
      $cookies.remove('XSRF-TOKEN', { path: '/' })
      $cookies.remove('ssid', { path: '/' })
      $cookies.remove('ssid.sig', { path: '/' })
      $window.location = '/auth/logout/'+`${sid}`
      setTimeout(function () {
        socket.disconnect()
      }, 100)
    }
  }
}
