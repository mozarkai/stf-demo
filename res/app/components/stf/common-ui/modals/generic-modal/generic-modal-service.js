/**
 * Copyright © 2019 code initially contributed by Orange SA, authors: Denis Barbaron - Licensed under the Apache license 2.0
 **/

module.exports = function GenericModalServiceFactory($uibModal, AppState) {
  const service = {};
  var UserService = {};
  var user = (UserService.currentUser = AppState.user);

  const ModalInstanceCtrl = function ($scope, $uibModalInstance, data, $http) {
    $scope.data = data;
    $scope.sessionData;
    $scope.heapMem = false;
    $scope.frames = false;
    $scope.memory = false;
    $scope.battery = false;
    $scope.cpu = false;
    $scope.har = false;
    $scope.video = false;
    $scope.videoUrl;
    $scope.queryparam = 'demo';

    if (data.har) {
      $scope.currentURL = window.location.href.split("control/");
      // const url = `http://app-interact-qa.mozark.ai:9090/mitm/${$scope.currentURL[1]}/start`;
      const url = `https://automation.mozark.ai:9091/mitm/${$scope.currentURL[1]}/start`;
      //const url = `https://hotstar-automation.mozark.ai:9091/mitm/${$scope.currentURL[1]}/start`;

      $http.get(url).then(function ({ data: { url } = {} }) {
        $uibModalInstance.close(url);
      });
    }

    if (data.sessionList) {
      const apiUrl = `https://api.mozark.ai/list?env=${$scope.queryparam}`;
      $http.post(apiUrl, {
        "eventName": "LIST Sessions",
        "eventAttributes": {

        },
        "mozark.eventAttributes": {
          "userId": user.email
        }
      }, { withCredentials: false }).then((response) => {
        $uibModalInstance.close(response);
      });
    }


    if (data.getPackage) {
      $scope.currentURL = window.location.href.split("control/");
      const apiUrl = `https://api.mozark.ai/package/${$scope.currentURL[1]}?env=${$scope.queryparam}`;
      $http.get(apiUrl, { withCredentials: false }).then((response) => {
        $uibModalInstance.close(response);
      });
    }

    if (data.startSession) {
      $scope.currentURL = window.location.href.split("control/");
      const apiUrl = `https://api.mozark.ai/capture?env=${$scope.queryparam}`;
      $http
        .post(
          apiUrl,
          {
            eventName: "Session Start",
            eventAttributes: {
              fileType: "all",
              action: "start",
              deviceId: $scope.currentURL[1],
              packageName: data.addPackage,
              batteryCapture: data.batteryCapture,
              cpuCapture: data.cpuCapture,
              framesCapture: data.framesCapture,
              harCapture: data.harCapture,
              heapCapture: data.heapCapture,
              memoryCapture: data.memoryCapture,
              videoCapture: data.videoCapture,
            },
            "mozark.eventAttributes": {
              userId: user.email,
            },
          },
          { withCredentials: false }
        )
        .then((response) => {
          $uibModalInstance.close(response);
        });
    }

    if (data.stopSession) {
      $scope.currentURL = window.location.href.split("control/");
      const apiUrl = `https://api.mozark.ai/capture?env=${$scope.queryparam}`;
      $http
        .post(
          apiUrl,
          {
            eventName: "Session Stop",
            eventAttributes: {
              fileType: "all",
              action: "stop",
              testId: data.addTestId,
              deviceId: $scope.currentURL[1],
            },
            "mozark.eventAttributes": {
              userId: user.email,
            },
          },
          {
            withCredentials: false,
          }
        )
        .then((response) => {
          $uibModalInstance.close(response);
        });
    }

    if (data.session) {
      $http
        .post(
          `https://api.mozark.ai/list?env=${$scope.queryparam}`,
          {
            eventName: "Session Information",
            eventAttributes: {
              testId: sessionStorage.getItem("ID"),
            },
            "mozark.eventAttributes": {
              userId: user.email,
            },
          },
          {
            withCredentials: false,
          }
        )
        .then((response) => {
          $scope.sessionData = response.data.list[0];
          sessionStorage.removeItem("ID");
          for (let i = 0; i < $scope.sessionData.url.length; i++) {
            if ($scope.sessionData.url[i]["device-resource.heap-mem"]) {
              $scope.heapMem = true;
            } else if ($scope.sessionData.url[i]["device-resource.frames"]) {
              $scope.frames = true;
            } else if ($scope.sessionData.url[i]["device-resource.mem"]) {
              $scope.memory = true;
            } else if ($scope.sessionData.url[i]["device-resource.battery"]) {
              $scope.battery = true;
            } else if ($scope.sessionData.url[i]["device-resource.cpu"]) {
              $scope.cpu = true;
            } else if ($scope.sessionData.url[i]["device-resource.har"]) {
              $scope.har = true;
            } else if ($scope.sessionData.url[i]["device-resource.video"]) {
              $scope.video = true;
              $scope.videoUrl = $scope.sessionData.url[i]["device-resource.video"]
            }
          }
        });
    }

    $scope.downloadFile = function (file, type) {
      if (type == 'video') {
        window.open($scope.videoUrl)
      }
      else {
        $http
          .get(
            $scope.sessionData.url
              .map((obj) => obj[file])
              .filter((obj) => obj != null)
              .toString()
          )
          .then((res) => {
            window.open(res.data.url);

          });
      }
      // window.open($scope.sessionData.url.map(obj => obj[file]).filter(obj => obj != null).toString())
    };

    $scope.ok = function () {
      $uibModalInstance.close("ok");
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss("cancel");
    };
  };

  service.open = function (data) {
    var modalInstance = $uibModal.open({
      template: require("./generic-modal.pug"),
      controller: ModalInstanceCtrl,
      size: data.size,
      animation: true,
      backdrop: "static",
      keyboard: false,
      resolve: {
        data: function () {
          return data;
        },
      },
    });

    return modalInstance.result;
  };

  return service;
};
