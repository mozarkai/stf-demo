/**
* Copyright © 2019 code initially contributed by Orange SA, authors: Denis Barbaron - Licensed under the Apache license 2.0
**/
require('./generic-modal.css')
module.exports = angular.module('stf.generic-modal', [
  require('stf/common-ui/modals/common').name,
  require('stf/app-state').name
])
  .factory('GenericModalService', require('./generic-modal-service'))
