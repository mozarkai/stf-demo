module.exports =
  function SocketDisconnectedServiceFactory($uibModal, $location, $window) {
    var service = {}

    var ModalInstanceCtrl = function($scope, $uibModalInstance, message) {
      $scope.ok = function() {
        $uibModalInstance.close(true)
        $window.location.reload()
      }

      $scope.message = message

      $scope.cancel = function() {
        $window.location.reload();
        $uibModalInstance.dismiss('cancel')
      }

    }

    service.open = function(message) {
      var modalInstance = $uibModal.open({
        template: require('./socket-disconnected.pug'),
        controller: ModalInstanceCtrl,
        resolve: {
          message: function() {
            return message
          }
        }
      })
      $window.location.reload();
      modalInstance.result.then(function() {
      }, function() {
      })
    }

    return service
  }
