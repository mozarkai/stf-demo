require("./session.css");

module.exports = angular
  .module("stf.session", [])
  .run([
    "$templateCache",
    function ($templateCache) {
      $templateCache.put("control-panes/session/session.pug", require("./session.pug"));
    },
  ])
  .controller("SessionCtrl", require("./session-controller"));
  require('stf/app-state').name

