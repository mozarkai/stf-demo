
module.exports = function SessionCtrl($scope, $http, AppState, GenericModalService) {

  $scope.showStartButton = !sessionStorage.getItem('session');
  $scope.showPackageDropdown = false
  $scope.data;
  $scope.packages;
  $scope.searchValue;
  $scope.currentURL = window.location.href.split("control/");
  const deviceID = $scope.currentURL[1];
  var UserService = {}

  var user = UserService.currentUser = AppState.user
  
  $scope.checkHar = function (){
    if($scope.har == false){
      $scope.har = true;
    }
    else{
      $scope.har = false;
    }
  }

  $scope.checkVideo = function (){
    if($scope.video == false){
      $scope.video = true;
    }
    else{
      $scope.video = false;
    }
  }

  $scope.checkHeap = function (){
    if($scope.heap == false){
      $scope.heap = true;
    }
    else{
      $scope.heap = false;
    }
  }

  $scope.checkFrame = function (){
    if($scope.frame == false){
      $scope.frame = true;
    }
    else{
      $scope.frame = false;
    }
  }

  $scope.checkMemory = function (){
    if($scope.memory == false){
      $scope.memory = true;
    }
    else{
      $scope.memory = false;
    }
  }

  $scope.checkBattery = function (){
    if($scope.battery == false){
      $scope.battery = true;
    }
    else{
      $scope.battery = false;
    }
  }

  $scope.checkCpu = function (){
    if($scope.cpu == false){
      $scope.cpu = true;
    }
    else{
      $scope.cpu = false;
    }
  }

  $scope.heap = true;
  $scope.frame = true;
  $scope.memory = true;
  $scope.battery = true;
  $scope.cpu = true;
  $scope.har = false;
  $scope.video = false;

  $scope.getPackages = function () {

    GenericModalService.open({
      message: "Please wait while packages finish loading ...",
      type: "Information",
      cancel: false,
      hideButton: true,
      getPackage: true
    }).then((response) => {
      if (response.data.status == 200) {
        $scope.showPackageDropdown = true;
        $scope.packages = response.data.list
      }
      else if (response.data.status != 200) {
        GenericModalService.open({
          message: response.data.message,
          type: "Error",
          cancel: true,
          hideButton: true,
          error: true
        });
      }
    });
  }

  $scope.setPackage = function (title) {
    $scope.searchValue = title;

    GenericModalService.open({
      message: "Do you want to start session recording?",
      type: "Confirmation",
      cancel: true,
      hideButton: false,
      addTitle: $scope.searchValue,
      startSessionConfirmation: true
    })
      .then(response => {
        console.log(response);
        if (response == 'ok') {
          start();
        }
      });
  }

  function start() {

    GenericModalService.open({
      message: "Starting session capture ...",
      type: "Information",
      cancel: false,
      hideButton: true,
      addPackage: $scope.searchValue,
      batteryCapture: $scope.battery,
      cpuCapture: $scope.cpu,
      framesCapture: $scope.frame,
      harCapture: $scope.har,
      heapCapture: $scope.heap,
      memoryCapture:$scope.memory,
      videoCapture:$scope.video,
      startSession: true
    })
      .then(response => {
        if (response.data.status == 200) {
          $scope.showStartButton = false;
          $scope.showPackageDropdown = false;
          $scope.data = response.data;
          sessionStorage.setItem('session', "sessionStart")
          sessionStorage.setItem('testId', $scope.data.testId)
        }
        else if (response.data.status != 200) {
          GenericModalService.open({
            message: response.data.message,
            type: "Error",
            cancel: true,
            hideButton: true,
            error: true
          });
        }
      })
  };

  $scope.end = function () {
    var setTestId = sessionStorage.getItem("testId")

    GenericModalService.open({
      message: "Stopping session capture...",
      type: "Information",
      cancel: false,
      hideButton: true,
      addTestId: setTestId,
      stopSession: true
    })
      .then(response => {
        $scope.showStartButton = true;
        $scope.data = response.data;
        sessionStorage.removeItem('session')
        sessionStorage.removeItem('testId')
        if (response.data.status != 200) {
          GenericModalService.open({
            message: response.data.message,
            type: "Error",
            cancel: true,
            hideButton: true,
            error: true
          });
        }
      })
  };

  $scope.filterFunction = function () {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    div = document.getElementById("myDropdown");
    a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
      txtValue = a[i].textContent || a[i].innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = "";
      } else {
        a[i].style.display = "none";
      }
    }
  }
};
