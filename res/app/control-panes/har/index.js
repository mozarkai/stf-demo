require("./har.css");

module.exports = angular
  .module("stf.har", [])
  .run([
    "$templateCache",
    function ($templateCache) {
      $templateCache.put("control-panes/har/har.pug", require("./har.pug"));
    },
  ])
  .controller("HarCtrl", require("./har-controller"));

