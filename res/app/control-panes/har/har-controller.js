module.exports = function HarCtrl(
  $sce,
  $scope,
  $http,
  UserService,
  GenericModalService
) {
  $scope.proxyLoading = false;
  $scope.currentURL = window.location.href.split("control/");
  $scope.harURL;
  $scope.deviceURL = UserService.deviceUrl;
  const deviceID = $scope.currentURL[1];
  $scope.mitm;
//https://demo-appfunctional.mozark.ai:8443/auth/devices/device/${deviceID}
//https://hotstar-automation.mozark.ai:8443/auth/devices/device/${deviceID}
  $http.get(`https://demo-appfunctional.mozark.ai:8443/auth/devices/device/${deviceID}`)
    .then(response => {
      $scope.mitm = response.data.json.response.data.mitm;
   })

   document.getElementById('clockdiv').style='display:none'
 
   //login for countdown timer for 05:00 
   function getTimeRemaining(endtime) {
     const total = Date.parse(endtime) - Date.parse(new Date());
     const seconds = Math.floor((total / 1000) % 60);
     const minutes = Math.floor((total / 1000 / 60) % 60);
     const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
     const days = Math.floor(total / (1000 * 60 * 60 * 24));
     
     return {
       total,
       days,
       hours,
       minutes,
       seconds
     };
   }
   
   function initializeClock(id, endtime) {
     const clock = document.getElementById(id);
     const minutesSpan = clock.querySelector('.minutes');
     const secondsSpan = clock.querySelector('.seconds');
   
     function updateClock() {
       const t = getTimeRemaining(endtime);
       minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
       secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
   
       if (t.total <= 0) {
         clearInterval(timeinterval);
       
       }
     }
   
     updateClock();
     const timeinterval = setInterval(updateClock, 1000);
   }

  $scope.getMitmURL = function (url) {
  //  $scope.harURL = true;
  //  $http.get($scope.deviceURL).then(function (response) {
      $scope.harURL = $sce.trustAsResourceUrl(url);
      // TODO: remove timeout after CORS issue is fixed
      setTimeout(() => {
        document.getElementById("iframeHAR").src = document.getElementById(
          "iframeHAR"
        ).src;
      }, 1500);
    //});
  };

  $scope.start = function start() {
    if (sessionStorage.getItem("session")) {
      GenericModalService.open({
        message: "Cannot start because session capture is on",
        type: "Information",
        cancel: true,
        hideButton: true,
      });
    } else {
      console.log("Start HAR capture", $scope);
      GenericModalService.open({
        message: "Please wait while the proxy is configured ...",
        type: "Information",
        cancel: false,
        hideButton: true,
        har: true,
      }).then((url) => {
        $scope.getMitmURL(url);
      });
    };
  }
//http://app-interact-qa.mozark.ai:9090/mitm/${deviceID}/stop
//https://demo-appfunctional.mozark.ai:9091/mitm/${deviceID}/stop
//https://hotstar-automation.mozark.ai:9091/mitm/${deviceID}/stop
  $scope.end = function end() {
    console.log("Stop HAR capture");
    $http
      .get(`https://automation.mozark.ai:9091/mitm/${deviceID}/stop`)
      .then(function (response) {
        $scope.harURL = null;
      });
  };
};
