module.exports = function DashboardCtrl($scope, DeviceService) {
    $scope.currentURL = window.location.href.split("control/");
    const deviceID = $scope.currentURL[1];
    $scope.iPhonePresent = deviceID.length > 23;
}
