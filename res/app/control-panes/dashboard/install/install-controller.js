module.exports = function InstallCtrl(
  $scope,
  InstallService,
  GenericModalService
) {
  $scope.currentURL = window.location.href.split("control/");
  $scope.iosDevice = $scope.currentURL[1].length > 23;
  $scope.accordionOpen = true;
  $scope.installation = null;
  $scope.clear = function () {
    $scope.installation = null;
    $scope.accordionOpen = false;
  };

  $scope.$on("installation", function (e, installation) {
    $scope.installation = installation.apply($scope);
  });

  $scope.installUrl = function (url) {
    return InstallService.installUrl($scope.control, url);
  };

  $scope.installFile = function ($files) {
    var fileName = $files[0].name;
    var format = fileName.split(".");
    var num = format.length - 1;
    var extension = format[num];
    //progress bar DOM
    var progressIPA = document.querySelector('#upload-ios-ipa')
    var progressBar = document.querySelector('#pgbar');
    var progressStatus = document.querySelector('#progress_ipa');
    var dropArea = document.querySelector('.drop-area')
    dropArea.style.display="none";
    progressBar.style.display="block"
    progressIPA.style.display = "none";
    document.querySelector('#loader-gif').src="https://i.gifer.com/ZZ5H.gif";

    if (extension == "ipa") {
      //TURN ON Progress bar
      progressIPA.style.display = "block";
      // Progress report
      progressBar.innerHTML = `<progress value="10" max="100" style="width:100%"></progress>`; // progress value
      progressStatus.innerText = `Upload Started...( 10% )` // progress message

      var formdata = new FormData();
      formdata.append("file", $files[0]);
      formdata.append("deviceId", $scope.currentURL[1]);

      // Progress report
      progressBar.innerHTML = `<progress value="30" max="100" style="width:100%"></progress>`; // progress value
      progressStatus.style.color = "black"
      progressStatus.innerText = `Uploading app...( 30% ).` // progress message

      var requestOptions = {
        method: "POST",
        body: formdata,
        redirect: "follow",
      };
      fetch(
        "https://automation.mozark.ai:17000/devices/appupload",
        requestOptions
      ).then((res) => {
        if (res.status == "200") {
          // Progress report
          progressBar.innerHTML = `<progress value="70" max="100" style="width:100%"></progress>`; // progress value
          progressStatus.style.color = "green"
          progressStatus.innerText = `Installing app...( 70% )` // progress message


          res.json().then(body=>{
          // Progress report
          progressBar.innerHTML = `<progress value="99" max="100" style="width:100%"></progress>`; // progress value
          progressStatus.style.color = "green"
          progressStatus.innerText = `Launching activity...( 99% )` // progress message
          progressIPA.style.display = "none";
          dropArea.style.display="block";

            GenericModalService.open({
            message: body.message,
            type: "Information",
            cancel: false,
            hideButton: false,
          });
          })
          
          
        } else {

          // Progress report
          progressBar.innerHTML = `<progress value="50" max="100" style="width:100%"></progress>`; // progress value
          progressStatus.style.color = "red"
          progressStatus.innerText = `Uninstalling app...( 50% )` // progress message
          dropArea.style.display="block";
          
          progressIPA.style.display = "none";
          GenericModalService.open({
            message: "Something went wrong",
            type: "Error",
            cancel: false,
            hideButton: false,
          });
        }
      }).catch((err)=>{
                  // Progress report
                  progressBar.innerHTML = `<progress value="10" max="100" style="width:100%"></progress>`; // progress value
                  progressStatus.style.color = "red"
                  progressStatus.innerText = `${err} . Try upload again ` // progress message
                  dropArea.style.display="block";
                  progressBar.style.display="none"
                  document.querySelector('#loader-gif').src="https://www.freeiconspng.com/uploads/error-icon-4.png";
                  // progressIPA.style.display = "none";
      });
    } else {
      if ($files.length) {
        return InstallService.installFile($scope.control, $files);
      }
    }
    // if ($files.length) {
    //   return InstallService.installFile($scope.control, $files)
    // }

    // var formdata = new FormData();
    // formdata.append("file", $files[0]);
    // formdata.append("deviceId", $scope.currentURL[1]);
    // var requestOptions = {
    //   method: 'POST',
    //   body: formdata,
    //   redirect: 'follow'
    // };
    // fetch("http://52.66.131.80:15000/api/appupload", requestOptions)
    //   .then(response => response.text())
    //   .then(result => console.log(result))
    //   .catch(error => console.log('error', error));
  };

  $scope.uninstall = function (packageName) {
    // TODO: After clicking uninstall accordion opens
    return $scope.control.uninstall(packageName).then(function () {
      $scope.$apply(function () {
        $scope.clear();
      });
    });
  };
};
