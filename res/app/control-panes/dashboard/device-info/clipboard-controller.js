module.exports = function ClipboardCtrl($scope, $http) {
//  $scope.clipboardContent = null
//
//  $scope.getClipboardContent = function () {
//    console.log('getting')
//
//    $scope.control.copy().then(function (result) {
//      $scope.$apply(function () {
//        if (result.success) {
//          if (result.lastData) {
//            $scope.clipboardContent = result.lastData
//          } else {
//            $scope.clipboardContent = gettext('No clipboard data')
//          }
//        } else {
//          $scope.clipboardContent = gettext('Error while getting data')
//        }
//      })
//    })
//  }

$scope.currentURL = window.location.href.split("control/");
const deviceID = $scope.currentURL[1];
$http.get('/api/v1/devices/' + deviceID).then(info=>{
    document.getElementById('info-device-serial').innerText=info.data.device.serial;
    document.getElementById('info-device-modelname').innerText=info.data.device.marketName;
    document.getElementById('info-device-manufacturer').innerText=info.data.device.manufacturer;
    document.getElementById('info-device-os').innerText=info.data.device.platform +" "+info.data.device.version;
    // document.getElementById('info-device-display').innerText=Number(info.data.device.display.size).toFixed(1);
    document.getElementById('info-device-carrier').innerText=info.data.device.operator;
    document.getElementById('info-device-mobile').innerText=info.data.device.phone.phoneNumber;
})

$scope.copyToClipboard = function() {
    let text = `Manufacturer : ${document.getElementById('info-device-manufacturer').innerText}\nModel Name :  ${document.getElementById('info-device-modelname').innerText}\nOS Version : ${document.getElementById('info-device-os').innerText}\nNetwork : ${document.getElementById('info-device-carrier').innerText}\nMobile Number : ${document.getElementById('info-device-mobile').innerText}\nDevice ID : ${document.getElementById('info-device-serial').innerText}`
    navigator.clipboard.writeText(text);
  }


}

