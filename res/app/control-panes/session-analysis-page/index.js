require("./sessionAnalysis.css");
module.exports = angular
  .module("stf.session-analysis", [])
  .run([
    "$templateCache",
    function ($templateCache) {
      $templateCache.put("control-panes/session-analysis-page/session-analysis.html", require("./session-analysis.html"));
    },
  ])
  .controller("SessionAnalysisCtrl", require("./session-analysis-controller"));
  require('stf/app-state').name
