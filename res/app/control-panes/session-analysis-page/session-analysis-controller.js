var Highcharts = require('highcharts');
module.exports = function SessionAnalysisCtrl($scope, AppState, $http, GenericModalService) {
  var UserService = {}
  $scope.sessionData
  $scope.currentURL = window.location.href.split("session/");
  const testID = $scope.currentURL[1];
  $scope.sharePopup = false;
  $scope.networkTabActive = false;
  $scope.kpiTabActive = false
  $scope.kpiHeightSet = 0;
  $scope.networkHeightSet = 0;
  $scope.kpiHeight = 0;
  $scope.networkHeight = 0;
  $scope.canvas;
  $scope.ctx;
  $scope.graphValue;
  $scope.videoTime = false;
  $scope.scaleGenerate = [0,15,29,44,58,73,87];

  var user = UserService.currentUser = AppState.user
  $http.post('https://api.mozark.ai/list', {
    "eventName": "Session Information",
    "eventAttributes": {
      testId: testID
    },
    "mozark.eventAttributes": {
      "userId": user.email
    }
  }, {
    withCredentials: false,
  })
    .then(resp => {
      if (resp.data.statusCode == 200) {
        $scope.sessionData = resp.data.list[0];
        $http.get($scope.sessionData.url.map(obj => obj['device-resource.har']).filter(obj => obj != null).toString()).then(res => {
          const url = res.data.url;
          window.perfCascade.getExampleHar(url);
        })
         $scope.videoTime = true;
        
      }
      else if (resp.data.statusCode != 200) {
        GenericModalService.open({
          message: resp.data.message,
          type: "Error",
          cancel: true,
          hideButton: true,
          error: true
        });
      }
    })

  $scope.openPcapsDownload = function () {
    $scope.sharePopup = !$scope.sharePopup;
  }

  $scope.downloadFile = function (file) {
    $http.get($scope.sessionData.url.map(obj => obj[file]).filter(obj => obj != null).toString()).then(res => {
      window.open(res.data.url)
    })
    // window.open($scope.sessionData.url.map(obj => obj[file]).filter(obj => obj != null).toString())
  }


  $scope.openCloseTab = function (str) {
    if (str == 'kpi') {
      $scope.kpiHeight = 200 + 'px'
      $scope.kpiTabActive = !$scope.kpiTabActive;
      $scope.kpiHeightSet = $scope.kpiTabActive ? $scope.kpiHeight : 0 + 'px';
    } else {
      $scope.networkHeight = 265 + 'px';
      $scope.networkTabActive = !$scope.networkTabActive
      $scope.networkHeightSet = $scope.networkTabActive ? $scope.networkHeight : 0 + 'px';
    }
  }

  var drawPieChart = function (){
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'pie1',
            type:'pie'
        },
        title: {
          text: null
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr']
        },
        credits: {
          enabled: false
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -30,
                    color:'white'
                }
            }
        },
        
        series: [{
            data: [29.9, 71.5, 106.4, 129.2]        
        }]
    });
}

var drawPieChart2 = function (){
  var chart = new Highcharts.Chart({
      chart: {
          renderTo: 'pie2',
          type:'pie'
      },
      title: {
        text: null
      },
      xAxis: {
          categories: ['Jan', 'Feb', 'Mar', 'Apr']
      },
      credits: {
        enabled: false
      },
      plotOptions: {
          series: {
              dataLabels: {
                  enabled: true,
                  formatter: function() {
                      return Math.round(this.percentage*100)/100 + ' %';
                  },
                  distance: -30,
                  color:'white'
              }
          }
      },
      
      series: [{
          data: [29.9, 71.5, 106.4, 129.2]        
      }]
  });
}

var drawPieChart3 = function (){
  var chart = new Highcharts.Chart({
      chart: {
          renderTo: 'pie3',
          type:'pie'
      },
      title: {
        text: null
      },
      xAxis: {
          categories: ['Jan', 'Feb', 'Mar', 'Apr']
      },
      credits: {
        enabled: false
      },
      plotOptions: {
          series: {
              dataLabels: {
                  enabled: true,
                  formatter: function() {
                      return Math.round(this.percentage*100)/100 + ' %';
                  },
                  distance: -30,
                  color:'white'
              }
          }
      },
      
      series: [{
          data: [29.9, 71.5, 106.4, 129.2]        
      }]
  });
}

drawPieChart();
drawPieChart2();
drawPieChart3();


  // var drawPieChart = function (data, colors, lcolors) {
  //   var canvas = document.getElementById('pie1');
  //   var ctx = canvas.getContext('2d');
  //   var x = canvas.width / 2.5;
  //   y = canvas.height / 2.5;
  //   var color,
  //     startAngle,
  //     endAngle,
  //     total = getTotal(data);

  //   for (var i = 0; i < data.length; i++) {
  //     color = colors[i];
  //     startAngle = calculateStart(data, i, total);
  //     endAngle = calculateEnd(data, i, total);
  //     radius = y - 100;
  //     var textSize = canvas.width / 10;
  //     middleAngle = startAngle + ((endAngle - startAngle) / 2);
  //     var posX = (radius / 2) * Math.cos(middleAngle) + x;
  //     var posY = (radius / 2) * Math.sin(middleAngle) + y;
  //     var w_offset = ctx.measureText(data[i]).width / 2;
  //     var h_offset = textSize / 4;
  //     ctx.beginPath();
  //     ctx.fillStyle = color;
  //     ctx.moveTo(x - 100, y - 25);
  //     ctx.arc(x - 100, y - 25, y - 100, startAngle, endAngle);
  //     ctx.fill();
  //     ctx.rect(canvas.width - 275, (y + 15) - i * 30, 12, 12);
  //     ctx.fill();
  //     ctx.font = "13px sans-serif";
  //     ctx.fillStyle = "#ffffff";
  //     ctx.fillText(calculatePercent(data[i].value, total) + '%', posX - w_offset - 75, posY + h_offset - 25)
  //     ctx.fillStyle = '#000000';
  //     ctx.fillText(data[i].label, canvas.width - 275 + 20, (y + 15) - i * 30 + 10);
  //   }
  // };

  // var drawPieChart2 = function (data, colors, lcolors) {
  //   var canvas = document.getElementById('pie2');
  //   var ctx = canvas.getContext('2d');
  //   var x = canvas.width / 2.5;
  //   y = canvas.height / 2.5;
  //   var color,
  //     startAngle,
  //     endAngle,
  //     total = getTotal(data);

  //   for (var i = 0; i < data.length; i++) {
  //     color = colors[i];
  //     startAngle = calculateStart(data, i, total);
  //     endAngle = calculateEnd(data, i, total);
  //     radius = y - 100;
  //     var textSize = canvas.width / 10;
  //     middleAngle = startAngle + ((endAngle - startAngle) / 2);
  //     var posX = (radius / 2) * Math.cos(middleAngle) + x;
  //     var posY = (radius / 2) * Math.sin(middleAngle) + y;
  //     var w_offset = ctx.measureText(data[i]).width / 2;
  //     var h_offset = textSize / 4;
  //     ctx.beginPath();
  //     ctx.fillStyle = color;
  //     ctx.moveTo(x - 100, y - 90);
  //     ctx.arc(x - 100, y - 90, y - 100, startAngle, endAngle);
  //     ctx.fill();
  //     ctx.rect(canvas.width - 275, (y - 50) - i * 30, 12, 12);
  //     ctx.fill();
  //     ctx.font = "13px sans-serif";
  //     ctx.fillStyle = "#ffffff";
  //     ctx.fillText(calculatePercent(data[i].value, total) + '%', posX - w_offset - 75, posY + h_offset - 90)
  //     ctx.fillStyle = '#000000';
  //     ctx.fillText(data[i].label, canvas.width - 275 + 20, (y - 50) - i * 30 + 10);
  //   }
  // };

  // var drawPieChart3 = function (data, colors, lcolors) {
  //   var canvas = document.getElementById('pie3');
  //   var ctx = canvas.getContext('2d');
  //   var x = canvas.width / 2.5;
  //   y = canvas.height / 2.5;
  //   var color,
  //     startAngle,
  //     endAngle,
  //     total = getTotal(data);

  //   for (var i = 0; i < data.length; i++) {
  //     color = colors[i];
  //     startAngle = calculateStart(data, i, total);
  //     endAngle = calculateEnd(data, i, total);
  //     radius = y - 100;
  //     var textSize = canvas.width / 10;
  //     middleAngle = startAngle + ((endAngle - startAngle) / 2);
  //     var posX = (radius / 2) * Math.cos(middleAngle) + x;
  //     var posY = (radius / 2) * Math.sin(middleAngle) + y;
  //     var w_offset = ctx.measureText(data[i]).width / 2;
  //     var h_offset = textSize / 4;
  //     ctx.beginPath();
  //     ctx.fillStyle = color;
  //     ctx.moveTo(x - 100, y - 60);
  //     ctx.arc(x - 100, y - 60, y - 100, startAngle, endAngle);
  //     ctx.fill();
  //     ctx.rect(canvas.width - 275, (y - 20) - i * 30, 12, 12);
  //     ctx.fill();
  //     ctx.font = "13px sans-serif";
  //     ctx.fillStyle = "#ffffff";
  //     ctx.fillText(calculatePercent(data[i].value, total) + '%', posX - w_offset - 75, posY + h_offset - 60)
  //     ctx.fillStyle = '#000000';
  //     ctx.fillText(data[i].label, canvas.width - 275 + 20, (y - 20) - i * 30 + 10);
  //   }
  // };

  // var calculatePercent = function (value, total) {

  //   return (value / total * 100).toFixed(0);
  // };

  // var getTotal = function (data) {
  //   var sum = 0;
  //   for (var i = 0; i < data.length; i++) {
  //     sum += data[i].value;
  //   }

  //   return sum;
  // };

  // var calculateStart = function (data, index, total) {
  //   if (index === 0) {
  //     return 0;
  //   }

  //   return calculateEnd(data, index - 1, total);
  // };

  // var calculateEndAngle = function (data, index, total) {
  //   var angle = data[index].value / total * 360;
  //   var inc = (index === 0) ? 0 : calculateEndAngle(data, index - 1, total);

  //   return (angle + inc);
  // };

  // var calculateEnd = function (data, index, total) {

  //   return degreeToRadians(calculateEndAngle(data, index, total));
  // };

  // var degreeToRadians = function (angle) {
  //   return angle * Math.PI / 180
  // }

  // var data = [
  //   { label: 'Connect', value: 50 },
  //   { label: 'Send', value: 40 },
  //   { label: 'Wait', value: 30 },
  //   { label: 'Recieve', value: 20 },
  //   { label: 'TLS', value: 10 }
  // ];
  // var colors = ['#ffc000', '#adb9ca', '#00b050', '#73b9ff', '#7030a0'];
  // var lcolors = ['#FFFF00', '#FFFF00', '#FFFF00', '#FFFF00', '#FFFF00'];

  // drawPieChart(data, colors, lcolors);
  // drawPieChart2(data, colors);
  // drawPieChart3(data, colors);

  (function (perfCascade) {

    /** holder DOM element to render PerfCascade into */
    var outputHolder = document.getElementsByClassName("output")[0];
    /** Select box for multi-page HARs */
    var pageSelectorEl = document.getElementById("page-selector");
    /** Holder element for legend HTML */
    var legendHolderEl = document.getElementById("legend-holder");

    /** options for PerfCascade (all have defaults)
     * Source: /src/ts/typing/options.d.ts
    */
    var perfCascadeOptions = {
      rowHeight: 23, //default: 23
      showAlignmentHelpers: true, //default: true
      showIndicatorIcons: true, //default: true
      leftColumnWidth: 25, //default: 25
      pageSelector: pageSelectorEl, //default: undefined
      legendHolder: legendHolderEl, //default: undefined (hide-legend)
      showUserTiming: true //default: false
    };

    /** renders the har (passing in the har.log node) */
    function renderPerfCascadeChart(harLogData) {
      /** remove all children of `outputHolder`,
       * so you can upload new HAR files and get a new SVG  */
      while (outputHolder.childNodes.length > 0) {
        outputHolder.removeChild(outputHolder.childNodes[0]);
      }

      /**
       * THIS IS WHERE THE MAGIC HAPPENS
       * pass HAR and options to `newPerfCascadeHar` to generate the SVG element
       */
      var perfCascadeSvg = perfCascade.fromHar(harLogData, perfCascadeOptions);

      /** append SVG to page - that's it */
      outputHolder.appendChild(perfCascadeSvg);
    }

    /** handle client side file upload */
    function onFileSubmit(evt) {
      var files = evt.target.files;
      if (!files) {
        alert("Failed to load HAR file");
        return
      }

      // USE THIS when not supporting compressed *.zhar files
      // var reader = new FileReader();
      // reader.onload = function(loadEvt){
      //   var harData
      //   try {
      //     harData = JSON.parse(loadEvt.target["result"]);
      //   } catch (err) {
      //     alert("File does not seem to be a valid HAR file");
      //     console.error(err)
      //     return undefined
      //   }
      //   renderPerfCascadeChart(harData.log);
      // };
      // reader.readAsText(files[0]);

      // Just needed for gzipped *.zhar files, you can use the standard FileReader api for normal .har files
      perfCascadeFileReader.readFile(files[0], evt.target.value, function (error, data) {
        if (error) {
          console.error(error)
        } else {
          renderPerfCascadeChart(data)
        }
      });
    }
    // document.getElementById("fileinput").addEventListener("change", onFileSubmit, false);



    /** functionality for "use example HAR" */
    function getExampleHar(url) {
      var xhr = new XMLHttpRequest();
      xhr.withCredentials = false;
      xhr.open('GET', url);
      xhr.addEventListener("readystatechange", function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
          var response = JSON.parse(xhr.responseText);
          renderPerfCascadeChart(response);
        }
      })
      xhr.send();
    }
    // document.getElementById("use-example").addEventListener("click", getExampleHar, false);
    perfCascade.getExampleHar = getExampleHar;
  })(perfCascade)



  charts = [];
  defaultOptions = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'areaspline'
    },
    title: {
      text: 'Browser market shares in January, 2018'
    },
    tooltip: {
      pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      series: {
        allowPointSelect: true,
      }
    },
    series: [
      {
        name: 'Brands',
        colorByPoint: true,
        data: [
          {
            name: 'Chrome',
            y: 61.41
          },
          {
            name: 'Internet Explorer',
            y: 11.84
          },
          {
            name: 'Firefox',
            y: 10.85
          },
          {
            name: 'Edge',
            y: 4.67
          },
          {
            name: 'Safari',
            y: 4.18
          },
          {
            name: 'Other',
            y: 2.61
          }
        ]
      }
    ]
  }

  var cpu = function () {
    Highcharts.chart('cpu', {
      chart: {
        type: 'areaspline',
      },
      title: {
        text: null
      },
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        title: {
          text: null
        }
      },
      yAxis: {
        title: {
          text: 'Time'
        },
        allowDecimals: true
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        series: {
          allowPointSelect: true,
          showInLegend: false,
        }
      },
      series: [{
        marker: {
          enabled: false
      },
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
      }]
    });

    //   Highcharts.chart('test', {
    //     chart: {
    //         type: 'areaspline'
    //     },
    //     // title: {
    //     //     text: 'Average fruit consumption during one week'
    //     // },
    //     legend: {
    //         layout: 'vertical',
    //         align: 'left',
    //         verticalAlign: 'top',
    //         x: 150,
    //         y: 100,
    //         floating: true,
    //         borderWidth: 1,
    //         backgroundColor:
    //             Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
    //     },
    //     xAxis: {
    //         categories: [
    //             '0','14','27','41','54','68','81'
    //         ],
    //         plotBands: [{ // visualize the weekend
    //             from: 4.5,
    //             to: 6.5,
    //             color: 'rgba(68, 170, 213, .2)'
    //         }]
    //     },
    //     yAxis: {
    //         title: {
    //             text: 'Time'
    //         },
    //         allowDecimals: true
    //     },
    //     tooltip: {
    //         shared: true,
    //         valueSuffix: ' units'
    //     },
    //     credits: {
    //         enabled: false
    //     },
    //     plotOptions: {
    //         areaspline: {
    //             fillOpacity: 0.5
    //         }
    //     },
    //     series: [{
    //         name: 'John',
    //         data: [3, 4, 3, 5, 4, 10, 12]
    //     }, 
    //     {
    //         name: 'Jane',
    //         data: [1, 3, 4, 3, 3, 5, 4]
    //     }]  
    // });
  }

  var memory = function () {
    Highcharts.chart('memory', {
      title: {
        text: null
      },
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        title: {
          text: 'Time'
        },
        allowDecimals: true
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        series: {
          allowPointSelect: true,
          showInLegend: false,
        }
      },
      series: [{
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
      }]
    });
  }

  var graphics = function () {
    Highcharts.chart('graphics', {
      title: {
        text: null
      },
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        title: {
          text: 'Time'
        },
        allowDecimals: true
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        series: {
          allowPointSelect: true,
          showInLegend: false,
        }
      },
      series: [{
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
      }]
    });
  }

  var battery = function () {
    Highcharts.chart('battery', {
      title: {
        text: null
      },
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        title: {
          text: 'Time'
        },
        allowDecimals: true
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        series: {
          allowPointSelect: true,
          showInLegend: false,
        }
      },
      series: [{
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
      }]
    });
  }

  var network = function () {
    Highcharts.chart('network', {
      title: {
        text: null
      },
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        title: {
          text: 'Time'
        },
        allowDecimals: true
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        series: {
          allowPointSelect: true,
          showInLegend: false,
          height: 10 + '%',
        }
      },
      series: [{
        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
      }]
    });
  }

  // var createChart = function () {
  //   let opts = this.defaultOptions;
  //   container = document.getElementById('test')
  //   console.log(opts);
  //   let e = document.createElement('div');

  //   container.appendChild(e);

  //   if (opts.chart) {
  //     // opts.chart['renderTo'] = e;
  //   }
  //   Highcharts.chart(container, opts);
  // }


  // var createChart2 = function () {
  //   let opts = this.defaultOptions;
  //   container = document.getElementById('test2')
  //   console.log(opts);
  //   let e = document.createElement('div');

  //   container.appendChild(e);

  //   if (opts.chart) {
  //     // opts.chart['renderTo'] = e;
  //   }
  //   Highcharts.chart(container, opts);
  // }

  // var createChart3 = function () {
  //   let opts = this.defaultOptions;
  //   container = document.getElementById('test3')
  //   console.log(opts);
  //   let e = document.createElement('div');

  //   container.appendChild(e);

  //   if (opts.chart) {
  //     // opts.chart['renderTo'] = e;
  //   }
  //   Highcharts.chart(container, opts);
  // }

  cpu();
  memory();
  graphics();
  battery();
  network();
}