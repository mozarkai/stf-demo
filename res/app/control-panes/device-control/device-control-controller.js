var _ = require("lodash");

module.exports = function DeviceControlCtrl(
  $scope,
  DeviceService,
  GroupService,
  $location,
  $timeout,
  $window,
  $rootScope,
  LogcatService,
  GenericModalService,
  $http
) {
  if (window !== window.parent) {
    document.getElementsByClassName("pane-top-bar")[0].style.display = "none";
  }

  window.addEventListener("message", receiveMessage, false);

  function receiveMessage(event) {
    if (event.data === "stop-using-device") {
      // safer for now since this also takes care of stopping propagation (see pug file)
      try {
        document.getElementsByClassName("btn-danger-outline")[0].click();
      } catch (e) {
        console.log(e);
      }
    }
  }

  var canvas = document.getElementById("canvas");
  const QRCode = window.QRCode;
  $scope.iPhonePresent = false;

  $scope.currentURL = window.location.href.split("control/");
  $scope.iPhonePresent =
    $scope.currentURL &&
    $scope.currentURL[1] &&
    $scope.currentURL[1].length > 23;
  $scope.showCanvas = false;

  $scope.closeQR = () => {
    $scope.showCanvas = false;
  };
  $scope.generateQR = () => {
    $scope.showCanvas = true;

    const location =
      window.location.href.replace("control/", "c/") + "?standalone";
    try {
      QRCode.toCanvas(canvas, location);
    } catch (err) {
      console.error(err);
    }
  };
  $scope.showScreen = true;

  $scope.groupTracker = DeviceService.trackGroup($scope);

  $scope.groupDevices = $scope.groupTracker.devices;
  console.log($scope.groupDevices);
  $scope.$on("$locationChangeStart", function (event, next, current) {
    $scope.LogcatService = LogcatService;
    $rootScope.LogcatService = LogcatService;
  });
  const deviceID = $scope.currentURL[1];
  $http.get(`https://automation.mozark.ai:9091/mitm/${deviceID}/stop`);
  $scope.kickDevice = function (device) {
    if (sessionStorage.getItem("session")) {
      var setTestId = sessionStorage.getItem("testId");
      GenericModalService.open({
        message: "Stopping session capture...",
        type: "Information",
        cancel: false,
        hideButton: true,
        addTestId: setTestId,
        stopSession: true,
      }).then((response) => {
        sessionStorage.removeItem("session");
        sessionStorage.removeItem("testId");
        kickDeviceOut(device);
        deviceLog = null;
        clearTimeout(timer);
      });
    } else {
      kickDeviceOut(device);
      deviceLog = null;
      clearTimeout(timer);
    }
    $http.get(`https://automation.mozark.ai:9091/mitm/${deviceID}/stop`);
  };

  let deviceLog = null;
  let timer;
  $scope.InactivityKickOutDevice = function (device) {
    if (device !== null && device !== deviceLog) {
      deviceLog = device;
    }
  };
  let inactivityKickout = function () {
    document.onmouseover = resetTimer;

    function inactivityDeviceKickout() {
      let device = deviceLog;
      if (sessionStorage.getItem("session")) {
        var setTestId = sessionStorage.getItem("testId");
        GenericModalService.open({
          message: "Stopping session capture...",
          type: "Information",
          cancel: false,
          hideButton: true,
          addTestId: setTestId,
          stopSession: true,
        }).then((response) => {
          sessionStorage.removeItem("session");
          sessionStorage.removeItem("testId");
          kickDeviceOut(device);
          deviceLog = null;
          clearTimeout(timer);
        });
      } else {
        kickDeviceOut(device);
        deviceLog = null;
        clearTimeout(timer);
      }
    }

    function resetTimer() {
      clearTimeout(timer);
      timer = setTimeout(inactivityDeviceKickout, 570000);
    }
  };
  inactivityKickout();

  function kickDeviceOut(device) {
    if (Object.keys(LogcatService.deviceEntries).includes(device.serial)) {
      LogcatService.deviceEntries[device.serial].allowClean = true;
    }

    $scope.LogcatService = LogcatService;
    $rootScope.LogcatService = LogcatService;

    if (!device || !$scope.device) {
      alert("No device found");
      return;
    }

    try {
      // If we're trying to kick current device
      if (device.serial === $scope.device.serial) {
        // If there is more than one device left
        if ($scope.groupDevices.length > 1) {
          // Control first free device first
          var firstFreeDevice = _.find($scope.groupDevices, function (dev) {
            return dev.serial !== $scope.device.serial;
          });
          $scope.controlDevice(firstFreeDevice);

          // Then kick the old device
          GroupService.kick(device).then(function () {
            $scope.$digest();
          });
        } else {
          // Kick the device
          GroupService.kick(device).then(function () {
            $scope.$digest();
          });
          $location.path("/devices/");
        }
      } else {
        GroupService.kick(device).then(function () {
          $scope.$digest();
        });
      }
    } catch (e) {
      alert(e.message);
    }
  }

  $scope.controlDevice = function (device) {
    $location.path("/control/" + device.serial);
  };

  function isPortrait(val) {
    var value = val;
    if (typeof value === "undefined" && $scope.device) {
      value = $scope.device.display.rotation;
    }
    return value === 0 || value === 180;
  }

  function isLandscape(val) {
    var value = val;
    if (typeof value === "undefined" && $scope.device) {
      value = $scope.device.display.rotation;
    }
    return value === 90 || value === 270;
  }

  $scope.tryToRotate = function (rotation) {
    if (rotation === "portrait") {
      $scope.control.rotate(0);
      $timeout(function () {
        if (isLandscape()) {
          $scope.currentRotation = "landscape";
        }
      }, 11400);
    } else if (rotation === "landscape") {
      $scope.control.rotate(90);
      $timeout(function () {
        if (isPortrait()) {
          $scope.currentRotation = "portrait";
        }
      }, 11400);
    }
  };

  // $scope.currentRotation = "portrait";

  $scope.$watch("device.display.rotation", function (newValue) {
    if (isPortrait(newValue)) {
      $scope.currentRotation = "portrait";
    } else if (isLandscape(newValue)) {
      $scope.currentRotation = "landscape";
    }
  });

  // TODO: Refactor this inside control and server-side
  $scope.rotateLeft = function () {
    var angle = 0;
    if ($scope.device && $scope.device.display) {
      angle = $scope.device.display.rotation;
    }
    if (angle === 0) {
      angle = 270;
    } else {
      angle -= 90;
    }
    $scope.control.rotate(angle);

    if ($rootScope.standalone) {
      $window.resizeTo($window.outerHeight, $window.outerWidth);
    }
  };

  $scope.rotateRight = function () {
    var angle = 0;
    if ($scope.device && $scope.device.display) {
      angle = $scope.device.display.rotation;
    }
    if (angle === 270) {
      angle = 0;
    } else {
      angle += 90;
    }
    $scope.control.rotate(angle);

    if ($rootScope.standalone) {
      $window.resizeTo($window.outerHeight, $window.outerWidth);
    }
  };
};
