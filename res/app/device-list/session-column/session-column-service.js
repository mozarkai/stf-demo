var _ = require('lodash')
var session = require('../session/device-list-session-directive');

var filterOps = {
  '<': function(a, filterValue) {
    return a !== null && a < filterValue
  }
, '<=': function(a, filterValue) {
    return a !== null && a <= filterValue
  }
, '>': function(a, filterValue) {
    return a !== null && a > filterValue
  }
, '>=': function(a, filterValue) {
    return a !== null && a >= filterValue
  }
, '=': function(a, filterValue) {
    return a !== null && a === filterValue
  }
}
module.exports = function SessionColumnService($filter, gettext, SettingsService, AppState, GenericModalService) {
  // Definitions for all possible values.
  return {
    testId: TextCell({
       title: gettext('Test ID')
    , value: function(data) {
       return data.testId
     }
    }),
    startDate: TextCell({
      title: gettext('Start Date')
    , value: function(data) {
       return data.startTime
     }
    }),
    duration: TextCell({
      title: gettext('Duration (s)')
    , value: function(data) {
       return data.duration
     }
    }),
    city: TextCell({
      title: gettext('City')
    , value: function(data) {
       return data.location
     }
    }),
    network: TextCell({
      title: gettext('AUT')
    , value: function(data) {
       return data.aut
     }
    }),
    carrier: TextCell({
      title: gettext('Carrier')
    , value: function(data) {
       return data.carrier
     }
    }),
    manufacturer: TextCell({
      title: gettext('Manufacturer')
    , value: function(data) {
       return data.manufacturer
     }
    }),
    model: TextCell({
      title: gettext('Model')
    , value: function(data) {
       return data.model
     }
    }),
    serial: TextCell({
      title: gettext('Serial')
    , value: function(data) {
       return data.deviceId
     }
    }),
    download: DeviceStatusCell({
      title: gettext('Download')
    , value: function(data) {
       return data.testId
     }
    }),
    // seeAnalysis: SeesionPage({
    //   title: gettext('See Analysis')
    // , value: function(data) {
    //    return data.testId
    //  }
    // })
  }
}

function DeviceStatusCell(options) {
  return _.defaults(options, {
    title: options.title
  , defaultOrder: 'asc'
  , build: function(btn, data) {
      var td = document.createElement('td')
     var a;
      if(btn){
        a = document.createElement("button");
        a.innerHTML = "Download";
        a.addEventListener ("click", function() {
          sessionStorage.setItem("ID", data.testId)
          btn.open({ 
          message: "Select file to download",
          type: "Test Logs",
          cancel: true,
          hideButton: true,
          session: true,
          testId : data.testId
        }).then((url) => {
         // $scope.getMitmURL(url);
        });
      })
    }
      else{
         a = document.createElement('a')
      
      a.appendChild(document.createTextNode(''))
      }
      td.appendChild(a)
      return td
    }
  , update: function(td, data) {
      var a = td.firstChild
      var t = a.firstChild

      a.className = 'btn btn-xs ' + 'btn-default-outline ' + 'state-ready btn-primary-outline'

    //  if (device.usable && !device.using) {
        // a.href = '#!/session/' + data.testId
       // GenericModalService.open()
     // }
     // else {
     //   a.removeAttribute('href')
      //}

      // t.nodeValue = options.value(device)
     // t.nodeValue = "download"
      return td
    }
  // , compare: (function() {
  //     var order = {
  //       using: 10
  //     , automation: 15
  //     , available: 20
  //     , busy: 30
  //     , ready: 40
  //     , preparing: 50
  //     , unauthorized: 60
  //     , offline: 70
  //     , present: 80
  //     , absent: 90
  //     }
  //     return function(deviceA, deviceB) {
  //       return order[deviceA.state] - order[deviceB.state]
  //     }
  //   })()
  // , filter: function(device, filter) {
  //     return device.state === filter.query
  //   }
   })
}

function SeesionPage(options) {
  return _.defaults(options, {
    title: options.title
  , defaultOrder: 'asc'
  , build: function() {
    var td = document.createElement('td')
    var a = document.createElement('a')
    a.appendChild(document.createTextNode(''))
    td.appendChild(a)
    return td
  }
, update: function(td, data) {
    var a = td.firstChild
    var t = a.firstChild

    a.className = 'btn btn-xs ' + 'btn-default-outline ' + 'state-ready btn-primary-outline'

    // if (device.usable && !device.using) {
    //   a.href = '#!/control/' + device.serial
    // }
    // else {
    //   a.removeAttribute('href')
    // }
    a.href = '#!/session/' + data.testId
    t.nodeValue = "See Analysis"
    return td
  }
  // , compare: (function() {
  //     var order = {
  //       using: 10
  //     , automation: 15
  //     , available: 20
  //     , busy: 30
  //     , ready: 40
  //     , preparing: 50
  //     , unauthorized: 60
  //     , offline: 70
  //     , present: 80
  //     , absent: 90
  //     }
  //     return function(deviceA, deviceB) {
  //       return order[deviceA.state] - order[deviceB.state]
  //     }
  //   })()
  // , filter: function(device, filter) {
  //     return device.state === filter.query
  //   }
   })
}


function TextCell(options) {
  return _.defaults(options, {
    title: options.title
  , defaultOrder: 'asc'
  , build: function() {
      var td = document.createElement('td')
      td.appendChild(document.createTextNode(''))
      return td
    }
  , update: function(td, item) {
      var t = td.firstChild
       t.nodeValue = options.value(item)
      return td
    }
  , compare: function(a, b) {
      return compareIgnoreCase(options.value(a), options.value(b))
    }
  , filter: function(item, filter) {
      return filterIgnoreCase(options.value(item), filter.query)
    }
  })
}