/**
 * Copyright © 2019 contains code contributed by Orange SA, authors: Denis Barbaron - Licensed under the Apache license 2.0
 **/

var QueryParser = require("./util/query-parser");

module.exports = function DeviceListCtrl(
  $scope,
  DeviceService,
  DeviceColumnService,
  GroupService,
  ControlService,
  SettingsService,
  $location,
  SessionColumnService,
  $http,
  $window
) {

  function countDevices() {
    $http.get("/api/v1/devices/").then((info) => {
      let available = info.data.devices.filter((device) => {
        if (device.present === true) {
          return device;
        }
      });
      // document.getElementById("deviceCounter1").innerText=available.filter(device=>{ if(device.platform==="iOS"){return device}}).length;
      // document.getElementById("deviceCounter2").innerText=available.filter(device=>{ if(device.platform==="Android"){return device}}).length;
    });
  }
  countDevices();
  $scope.tracker = DeviceService.trackAll($scope);
  $scope.control = ControlService.create($scope.tracker.devices, "*ALL");

  $scope.columnDefinitions = DeviceColumnService;

  $scope.sessionColumnDefinitions = SessionColumnService;
  var defaultColumns = [
    {
      name: "state",
      selected: true,
    },
    {
      name: "manufacturer",
      selected: true,
    },
    {
      name: "marketName",
      selected: true,
    },
    {
      name: "version",
      selected: true,
    },
    {
      name: "operator",
      selected: true,
    },
    {
      name: "phone",
      selected: true,
    },
    {
      name: "display",
      selected: false,
    },
    {
      name: "serial",
      selected: true,
    },
    {
      name: "model",
      selected: false,
    },
    {
      name: "name",
      selected: false,
    },
    {
      name: "releasedAt",
      selected: false,
    },
    // , {
    // name: 'network'
    // , selected: true
    // }
    {
      name: "sdk",
      selected: false,
    },
    {
      name: "abi",
      selected: false,
    },
    {
      name: "cpuPlatform",
      selected: false,
    },
    {
      name: "openGLESVersion",
      selected: false,
    },
    {
      name: "browser",
      selected: false,
    },
    /*
    {
      name: "imei",
      selected: true,
    },
    */
    {
      name: "imsi",
      selected: false,
    },
    {
      name: "iccid",
      selected: false,
    },
    {
      name: "batteryHealth",
      selected: false,
    },
    {
      name: "batterySource",
      selected: false,
    },
    {
      name: "batteryStatus",
      selected: false,
    },
    {
      name: "batteryLevel",
      selected: false,
    },
    {
      name: "batteryTemp",
      selected: false,
    },
    {
      name: "provider",
      selected: true,
    },
    {
      name: "notes",
      selected: false,
    },
    {
      name: "owner",
      selected: false,
    },
    {
      name: "group",
      selected: false,
    },
    {
      name: "groupSchedule",
      selected: false,
    },
    {
      name: "groupStartTime",
      selected: false,
    },
    {
      name: "groupEndTime",
      selected: false,
    },
    {
      name: "groupRepetitions",
      selected: false,
    },
    {
      name: "groupOwner",
      selected: false,
    },
    {
      name: "groupOrigin",
      selected: false,
    },
  ];

  var sessionDefaultColumns = [
    {
      name: "testId",
      selected: true,
    },
    {
      name: "startDate",
      selected: true,
    },
    {
      name: "duration",
      selected: true,
    },
    {
      name: "city",
      selected: true,
    },
    {
      name: "network",
      selected: true,
    },
    {
      name: "carrier",
      selected: true,
    },
    {
      name: "manufacturer",
      selected: true,
    },
    {
      name: "model",
      selected: true,
    },
    {
      name: "serial",
      selected: true,
    },
    {
      name: "download",
      selected: true,
    },
    // , {
    //   name: 'seeAnalysis'
    //   , selected: true
    // }
  ];

  $scope.sessionColumns = sessionDefaultColumns;
  $scope.columns = defaultColumns;
  SettingsService.bind($scope, {
    target: "columns",
    source: "deviceListColumns",
  });

  var defaultSort = {
    fixed: [
      {
        name: "state",
        order: "asc",
      },
    ],
    user: [
      {
        name: "name",
        order: "asc",
      },
    ],
  };

  $scope.sort = defaultSort;

  SettingsService.bind($scope, {
    target: "sort",
    source: "deviceListSort",
  });

  $scope.filter = [];

  $scope.activeTabs = {
    icons: true,
    details: false,
    session: false,
  };

  SettingsService.bind($scope, {
    target: "activeTabs",
    source: "deviceListActiveTabs",
  });

  $scope.toggle = function (device) {
    if (device.using) {
      $scope.kick(device);
    } else {
      $location.path("/control/" + device.serial);
    }
  };

  $scope.invite = function (device) {
    return GroupService.invite(device).then(function () {
      $scope.$digest();
    });
  };

  $scope.applyFilter = function (query) {
    $scope.filter = QueryParser.parse(query);
  };

  $scope.search = {
    deviceFilter: "",
    focusElement: false,
  };

  $scope.focusSearch = function () {
    if (!$scope.basicMode) {
      $scope.search.focusElement = true;
    }
  };

  $scope.reset = function () {
    $scope.search.deviceFilter = "";
    $scope.filter = [];
    $scope.sort = defaultSort;
    $scope.columns = defaultColumns;
  };
};
