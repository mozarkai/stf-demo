require('./device-list-session.css')

module.exports = angular.module('stf.device-list.session', [
  require('stf/device').name,
  require('stf/user/group').name,
  require('stf/common-ui').name,
  require('stf/admin-mode').name,
  require('../column').name,
  require('../empty').name,
  require('../session-column').name,
  require('stf/app-state').name
])
  .directive('deviceListSession', require('./device-list-session-directive'))