/**
 * Copyright © 2019 contains code contributed by Orange SA, authors: Denis Barbaron - Licensed under the Apache license 2.0
 **/

module.exports = function SignInCtrl($scope, $http, CommonService, $window) {
  window.addEventListener("message", receiveMessage, false);

  function receiveMessage(event) {
    // Check if the message is coming from the main window
    /* if (event.origin !== "http://example.com") return; */

    // Access the authentication information from the message data
    try {
      var { username, password } = event.data;

      if (!username || !password) return undefined;

      // Populate the input fields with the authentication information
      document.getElementById("username").value = username;
      document.getElementById("password").value = password;
      angular
        .element(document.querySelector("#username"))
        .triggerHandler("input");
      angular
        .element(document.querySelector("#password"))
        .triggerHandler("input");
      // Automatically click the submit button
      document.getElementsByClassName("btn-block")[0].click();
    } catch (e) { console.log(e) }
  }

  let path = window.location.href;
  let standalonePath = false;
  if (window.location.href.slice(-10) === "standalone") {
    standalonePath =
      window.location.origin +
      "/" +
      path.split("/").pop().trim().replaceAll("%2F", "/").replace("%3F", "?");
  }
  $scope.error = null;

  $scope.submit = function() {
    var data = {
      name: $scope.signin.username.$modelValue,
      email: $scope.signin.email.$modelValue,
    };

    if (standalonePath) {
      data.standalonePath = standalonePath;
    }
    $scope.invalid = false;
    // alert("give your MFA code")
    $http
      .post("/auth/api/v1/mock", data)
      .success(function(response) {
        $scope.error = null;
        // console.log(response.redirect)
        let cognito = require("../../../cognito/config/accounts");
        new Promise((resolve) => {
          cognito.authenticate(data.name, data.email).then((result) => {
            // cognito.getMFAStatus(data.email)
            resolve(result);
            location.replace(response.redirect);
          });
        });
      })
      .error(function(response) {
        switch (response.error) {
          case "ValidationError":
            $scope.error = {
              $invalid: true,
            };
            break;
          case "InvalidCredentialsError":
            $scope.error = {
              $incorrect: true,
            };
            break;
          default:
            $scope.error = {
              $server: true,
            };
            break;
        }
      });
  };

  $scope.mailToSupport = function() {
    CommonService.url("mailto:" + $scope.contactEmail);
  };

  $http.get("/auth/contact").then(function(response) {
    $scope.contactEmail = response.data.contact.email;
  });
}
