const AWS =require('aws-sdk')
// import { CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js'
var AmazonCognitoIdentity = require("amazon-cognito-identity-js");
const Pool = require('../config/userpool.js')

const cognito = new AWS.CognitoIdentityServiceProvider({ region: 'ap-south-1' })

 //Get session details
  const getSession =  () =>
     new Promise((resolve, reject) => {
      const user = Pool.getCurrentUser()
      if (user) {
        user.getSession( (err, session) => {
          if (err) {
            reject()
          } else {
            const attributes =  new Promise((resolve, reject) => {
              user.getUserAttributes((err, attributes) => {
                if (err) {
                  reject(err)
                } else {
                  console.log('attributes:', attributes)
                  const results = {}

                  for (let attribute of attributes) {
                    const { Name, Value } = attribute
                    results[Name] = Value
                  }

                  resolve(results)
                }
              })
            })

            const accessToken = session.accessToken.jwtToken

            const mfaEnabled =  new Promise((resolve) => {
              cognito.getUser(
                {
                  AccessToken: accessToken,
                },
                (err, data) => {
                  if (err) resolve(false)
                  else
                    resolve(
                      data.UserMFASettingList &&
                        data.UserMFASettingList.includes('SOFTWARE_TOKEN_MFA')
                    )
                }
              )
            })

            const token = session.getIdToken().getJwtToken()

            resolve({
              user,
              accessToken,
              mfaEnabled,
              headers: {
                'x-api-key': attributes['custom:apikey'],
                Authorization: token,
              },
              session,
              attributes,
            })
          }
        })
      } else {
        reject()
      }
    })

//authentication 
  const authenticate =  (Username, Password) =>
     new Promise((resolve, reject) => {
      const user = new AmazonCognitoIdentity.CognitoUser({ Username, Pool })
      const authDetails = new AmazonCognitoIdentity.AuthenticationDetails({ Username, Password })

      user.authenticateUser(authDetails, {
        onSuccess: (data) => {
          console.log('onSuccess:', data)
          resolve(data)
        },

        onFailure: (err) => {
          console.error('onFailure:', err)
          reject(err)
        },

        newPasswordRequired: (data) => {
          console.log('newPasswordRequired:', data)
          resolve(data)
        },

        totpRequired: () => {
          const token = prompt('Please enter your 6-digit TOTP')
          user.sendMFACode(
            token,
            {
              onSuccess: () => resolve(),
              onFailure: () => alert('Incorrect code!'),
            },
            'SOFTWARE_TOKEN_MFA'
          )
        },
      })
    })


//logout
  const logout = () => {
    const user = Pool.getCurrentUser()
    if (user) {
      user.signOut()
    }
  }

module.exports = {
    getSession,
    authenticate,
    logout
  };
  