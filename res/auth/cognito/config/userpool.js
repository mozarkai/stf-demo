var AmazonCognitoIdentity = require("amazon-cognito-identity-js");

const poolData = {
  UserPoolId: "ap-south-1_BetfnCqly",
  ClientId: "5gtqfj6hqmpq9h2eea7s87pr9e",
  Storage: new AmazonCognitoIdentity.CookieStorage({domain:"localhost" , secure:"false"})
}

module.exports = new AmazonCognitoIdentity.CognitoUserPool(poolData);
