/**
 * Copyright © 2019 contains code contributed by Orange SA, authors: Denis Barbaron - Licensed under the Apache license 2.0
 **/

module.exports = function SignInCtrl($scope, $http, CommonService) {
  let path = window.location.href;
  let standalonePath = false;
  if (window.location.href.slice(-10) === "standalone") {
    standalonePath =
      window.location.origin +
      "/" +
      path.split("/").pop().trim().replaceAll("%2F", "/").replace("%3F", "?");
  }
  $scope.error = null;

  $scope.submit = function () {
    var data = {
      name: $scope.signin.username.$modelValue,
      email: $scope.signin.email.$modelValue,
    };
    if (standalonePath) {
      data.standalonePath = standalonePath;
    }
    $scope.invalid = false;
    // alert("give your MFA code")
    $http
      .post("/auth/api/v1/cognito", data)
      .success(function (response) {
        $scope.error = null;
        // console.log(response.redirect)
        let cognito = require("../../../../../lib/units/auth/cognito/index");
        new Promise((resolve) => {
          cognito.signIn(data.name, data.email).then((result) => {
            // cognito.getMFAStatus(data.email)
            resolve(result);
            location.replace(response.redirect);
          });
        });
      })
      .error(function (response) {
        switch (response.error) {
          case "ValidationError":
            $scope.error = {
              $invalid: true,
            };
            break;
          case "InvalidCredentialsError":
            $scope.error = {
              $incorrect: true,
            };
            break;
          default:
            $scope.error = {
              $server: true,
            };
            break;
        }
      });
  };

  $scope.mailToSupport = function () {
    CommonService.url("mailto:" + $scope.contactEmail);
  };

  $http.get("/auth/contact").then(function (response) {
    $scope.contactEmail = response.data.contact.email;
  });
};
