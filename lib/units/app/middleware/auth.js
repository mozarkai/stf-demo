/**
 * Copyright © 2019 contains code contributed by Orange SA, authors: Denis Barbaron - Licensed under the Apache license 2.0
 **/

var jwtutil = require("../../../util/jwtutil");
var urlutil = require("../../../util/urlutil");

var dbapi = require("../../../db/api");

module.exports = function (options) {
  return function (req, res, next) {
    if (req.query.jwt) {
      let redir;
      let standalonePath = false;
      // Coming from auth client
      var data = jwtutil.decode(req.query.jwt, options.secret);
      if (req.query.standalonePath) {
        standalonePath = req.query.standalonePath;
      } else {
        redir = urlutil.removeParam(req.url, "jwt");
      }
      // redir = urlutil.removeParam(req.url, "standalone");
      // redir = urlutil.removeParam(req.url, "standalonePath");
      if (data) {
        // Redirect once to get rid of the token
        dbapi
          .saveUserAfterLogin({
            name: data.name,
            email: data.email,
            ip: req.ip,
          })
          .then(function () {
            req.session.jwt = data;
            req.sessionOptions.httpOnly = false;
            console.log({ standalonePath, redir });
            console.log(options.appUrl, options.authUrl);
            res.redirect(standalonePath || redir);
          })
          .catch(next);
      } else {
        // Invalid token, forward to auth client
        res.redirect(options.authUrl);
      }
    } else if (req.session && req.session.jwt) {
      dbapi
        .loadUser(req.session.jwt.email)
        .then(function (user) {
          if (user) {
            // Continue existing session
            req.user = user;
            next();
          } else {
            // We no longer have the user in the database
            res.redirect(options.authUrl);
          }
        })
        .catch(next);
    } else {
      // No session, forward to auth client
      res.redirect(options.authUrl);
    }
  };
};
