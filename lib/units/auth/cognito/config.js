const AWS = require("aws-sdk");
const jwt_decode = require("jwt-decode");
const AmazonCognitoIdentity = require("amazon-cognito-identity-js");
let cognitoAttributeList = [];
var AWS_COGNITO_USER_POOL_ID = "ap-south-1_d0DERUUj3";
var AWS_COGNITO_CLIENT_ID = "56hmjjks0ua50pr0vscc1l878f";
var AWS_COGNITO_REGION = "ap-south-1";
var AWS_COGNITO_IDENTITY_POOL_ID =
  "ap-south-1:7bcd9e1f-f392-4802-9ec1-5f56c15ffa37";

const poolData = {
  UserPoolId: AWS_COGNITO_USER_POOL_ID,
  ClientId: AWS_COGNITO_CLIENT_ID,
  Storage: new AmazonCognitoIdentity.CookieStorage({domain:"localhost" , secure:"false"})
};

const attributes = (key, value) => {
  return {
    Name: key,
    Value: value,
  };
};

function setCognitoAttributeList(email, agent) {
  let attributeList = [];
  attributeList.push(attributes("email", email));
  attributeList.forEach((element) => {
    cognitoAttributeList.push(
      new AmazonCognitoIdentity.CognitoUserAttribute(element)
    );
  });
}

function getCognitoAttributeList() {
  return cognitoAttributeList;
}

function getCognitoUser(email) {
  const userData = {
    Username: email,
    Pool: getUserPool(),
    Storage: new AmazonCognitoIdentity.CookieStorage({domain:"localhost" , secure:"false"})
  };
  return new AmazonCognitoIdentity.CognitoUser(userData);
}

function getUserPool() {
  return new AmazonCognitoIdentity.CognitoUserPool(poolData);
}


function getAuthDetails(email, password) {
  var authenticationData = {
    Username: email,
    Password: password,
  };
  return new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
}

function initAWS(
  region = AWS_COGNITO_REGION,
  identityPoolId = AWS_COGNITO_IDENTITY_POOL_ID
) {
  AWS.config.region = region; // Region
  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: identityPoolId,
  });
}

function decodeJWTToken(token) {
  const { email, exp, auth_time, token_use, sub } = jwt_decode(token.idToken);
  return { token, email, exp, uid: sub, auth_time, token_use };
}

// const getSession = async () =>{
// await new Promise((resolve, reject) => {
//   const user = Pool.getCurrentUser()
//   if (user) {
//     user.getSession(async (err, session) => {
//       if (err) {
//         reject()
//       } else {
//         const attributes = await new Promise((resolve, reject) => {
//           user.getUserAttributes((err, attributes) => {
//             if (err) {
//               reject(err)
//             } else {
//               console.log('attributes:', attributes)
//               const results = {}

//               for (let attribute of attributes) {
//                 const { Name, Value } = attribute
//                 results[Name] = Value
//               }

//               resolve(results)
//             }
//           })
//         })

//         const accessToken = session.accessToken.jwtToken

//         const mfaEnabled = await new Promise((resolve) => {
//           cognito.getUser(
//             {
//               AccessToken: accessToken,
//             },
//             (err, data) => {
//               if (err) resolve(false)
//               else
//                 resolve(
//                   data.UserMFASettingList &&
//                     data.UserMFASettingList.includes('SOFTWARE_TOKEN_MFA')
//                 )
//             }
//           )
//         })

//         const token = session.getIdToken().getJwtToken()

//         resolve({
//           user,
//           accessToken,
//           mfaEnabled,
//           headers: {
//             'x-api-key': attributes['custom:apikey'],
//             Authorization: token,
//           },
//           ...session,
//           ...attributes,
//         })
//       }
//     })
//   } else {
//     reject()
//   }
// })}

module.exports = {
  initAWS,
  getCognitoAttributeList,
  getUserPool,
  getCognitoUser,
  setCognitoAttributeList,
  getAuthDetails,
  decodeJWTToken,
};
