const AwsConfig = require("./config");
function signUp(email, password, agent = "none") {
  return new Promise((resolve) => {
    AwsConfig.initAWS();
    AwsConfig.setCognitoAttributeList(email, agent);
    AwsConfig.getUserPool().signUp(
      email,
      password,
      AwsConfig.getCognitoAttributeList(),
      null,
      function (err, result) {
        if (err) {
          return resolve({ statusCode: 422, response: err });
        }
        const response = {
          username: result.user.username,
          userConfirmed: result.userConfirmed,
          userAgent: result.user.client.userAgent,
        };
        return resolve({ statusCode: 201, response: response });
      }
    );
  });
}

function verify(email, code) {
  return new Promise((resolve) => {
    AwsConfig.getCognitoUser(email).confirmRegistration(
      code,
      true,
      (err, result) => {
        if (err) {
          return resolve({ statusCode: 422, response: err });
        }
        return resolve({ statusCode: 400, response: result });
      }
    );
  });
}

function signIn(email, password) {
  return new Promise((resolve,reject) => {
    AwsConfig.getCognitoUser(email).authenticateUser(
      AwsConfig.getAuthDetails(email, password),
      {
        onSuccess: (result) => {
          const token = {
            accessToken: result.getAccessToken().getJwtToken(),
            idToken: result.getIdToken().getJwtToken(),
            refreshToken: result.getRefreshToken().getToken(),
          };
          return resolve({
            statusCode: 200,
            response: AwsConfig.decodeJWTToken(token),
          });
        },

        onFailure: (err) => {
          return resolve("unauthenticated");
        }
      }
    );
  });
}
function logout(email){
  AwsConfig.getCognitoUser(email).signOut()
}
// function setupTOTP(email){
//   return new Promise((resolve,reject)=>{
//     var cognitoUser = AwsConfig.getUserPool.getCurrent
//   }

//   )
// }
function getMFAStatus(email){
  AwsConfig.getCognitoUser(email).getUserData((err, data) => {
    if (err) {
      alert(err.message || JSON.stringify(err));
      return
    }
    const { PreferredMfaSetting, UserMFASettingList } = data;
    alert(
      JSON.stringify({ PreferredMfaSetting, UserMFASettingList }, null, 2)
    )
  })
}

module.exports = {
  signUp,
  verify,
  signIn,
  logout,
  getMFAStatus
};
